\chapter{Mod\`ele \`a \'etat continu}
\label{ch:modele_continu}

\section{D\'efinition des param\`etres du mod\`ele et relations entre les param\`etres}

La grandeur caract\'eristique utile pour \'evaluer l'acquisition du cours de \textit{Mod\'elisation} est la connaissance acquise par l'\'etudiant, que l'on note $C$. On suppose que la connaissance est born\'ee entre $0$ et $C_{max}$ (où $C_{max}$ correspond à la connaissance de l'intégralité du cours). La mod\'elisation de la motivation d'apprendre permet de d\'eterminer l'\'evolution de la connaissance $C$ au cours du temps, ce qui justifie la n\'ecessit\'e de cette mod\'elisation.

On d\'efinit ensuite l'apprentissage $A$ comme $A = \frac{dC}{dt}$. En effet, intuitivement, l'apprentissage correspond \`a une vitesse d'\'evolution de la connaissance.

La motivation, not\'ee $U$, est d\'efinie comme une grandeur comprise entre $0$ et $1$, $0$ correspondant \`a une absence totale de volont\'e de travailler et $1$ \`a une volont\'e de travail in\'ebranlable. D'apr\`es Steel et Koenig \cite{SteelKoenig}, la motivation est donn\'ee par la loi qui suit :

\begin{equation}
\label{eq:U}
U = \frac{E_{res}V_{res}}{Z + \Gamma(T-t)}
\end{equation}

o\`u :
\begin{itemize}
 \item $V_{res}$ est la valeur ressentie par l'\'etudiant du travail qu'il doit fournir
 \item $E_{res}$ est la probabilit\'e de r\'eussite ressentie de l'\'etudiant
 \item $T$ est l'instant avant lequel le travail doit \^etre effectu\'e, par exemple la date de l'examen du cours de \textit{Mod\'elisation}
 \item $\Gamma$ est l'impulsivit\'e de l'\'etudiant, qui caract\'erise son go\^ut pour la procrastination
 \item $Z$ est une constante strictement positive qui emp\^eche $U$ de diverger lorsque $t$ tend vers $T$. On peut assimiler $Z$ \`a la valeur maximale d'un travail.
\end{itemize}

La probabilit\'e de r\'eussite ressentie $E_{res}$ peut \^etre exprim\'ee en fonction de la probabilit\'e de r\'eussite r\'eelle $E$ d'apr\`es Steel et Koenig \cite{SteelKoenig} :

\begin{equation}
E_{res} = \frac{E^\gamma}{E^\gamma + (1 - E^\gamma)^\frac{1}{\gamma}}
\end{equation}

avec $\gamma = 0,6$.

Pour d\'eterminer la probabilit\'e de r\'eussite r\'eelle de l'\'etudiant, nous avons d\'efini de nouvelles grandeurs :

\begin{itemize}
 \item $D$ la difficult\'e du cours, suppos\'ee constante;
 \item $N$ le niveau de l'\'etudiant, qui cro\^it avec la connaissance de l'\'etudiant.
\end{itemize}

Nous avons choisi une loi affine pour repr\'esenter la croissance de $N$ avec $C$ :

\begin{equation}
N = N_{min} + C \frac{N_{max} - N_{min}}{C_{max}}
\end{equation}

o\`u $N_{min}$ est le niveau de d\'epart de l'\'etudiant et $N_{max}$ est son niveau maximal, atteint si sa comp\'etence devient maximale.

Les constantes $N_{max}$ et $D$ doivent \^etre du m\^eme ordre de grandeur si le niveau du cours est adapt\'e \`a celui de l'\'etudiant, ce qui correspond \`a une probabilit\'e de r\'eussite sup\'erieure \`a $\frac{1}{2}$.

La loi pour exprimer la probabilit\'e de r\'eussite $E$ en fonction de $\frac{N}{D}$ doit v\'erifier 3 crit\`eres principaux :

\begin{itemize}
 \item $E$ est proportionnel \`a $\frac{N}{D}$ pour $\frac{N}{D} << 1$;
 \item $\lim_{\frac{N}{D}\to\infty} E(\frac{N}{D}) = 1 $;
 \item $E(\frac{N}{D} = 1) = \frac{3}{4}$.
\end{itemize}

Ainsi, nous avons opt\'e pour la loi suivante, qui respecte ces trois propri\'et\'es :

\begin{equation}
E(\frac{N}{D}) = 1 - e^{-\alpha\frac{N}{D}}
\end{equation}

avec $\alpha = \ln(4)$.


\section{Repr\'esentation d'\'etat}

Le vecteur d'entr\'ee choisi pour la repr\'esentation d'\'etat est

\begin{equation}
\textbf{u} = \begin{pmatrix}
      Z & C_{max} & A_{max} & \Gamma & N_{min} & N_{max} & V & D & T
     \end{pmatrix}^T
\end{equation}

Il contient les informations de toutes les constantes n\'ecessaires \`a la r\'esolution des \'equations.
$\textbf{u}$ est ind\'ependant du temps.
$Z$ est une variable globale du syst\`eme, tandis que $C_{max}$, $D$ et $T$ sont des constantes d\'ependant du cours, $A_{max}$, $\Gamma$, $N_{min}$ et $N_{max}$ sont des constantes qui d\'ependent de l'\'etudiant, et $V$ d\'epend du cours et de l'\'etudiant.

La variable de sortie est $C$. C'est la grandeur qu'on cherche \`a d\'eterminer.

Le vecteur d'\'etat est

\begin{equation}
\textbf{x} = \begin{pmatrix}
      C & E & E_{res}
     \end{pmatrix}^T
\end{equation}
     
Il v\'erifie l'\'equation diff\'erentielle du premier ordre

\begin{equation}
\label{eq:f}
\frac{d\textbf{x}}{dt} =
\begin{pmatrix}
 \frac{dC}{dt} \\ \frac{dE}{dt} \\ \frac{dE_{res}}{dt}
\end{pmatrix} =
f(\textbf{x}(t), \textbf{u}, t) =
\begin{pmatrix}
 \frac{A_{max}E_{res}V}{Z + \Gamma(T-t)}
 \\
 (1-E)\frac{\alpha}{D}\frac{N_{max}-N_{min}}{C_{max}}\frac{dC}{dt}
 \\
 \frac{\frac{dE}{dt}E^{\gamma-1}(1-E^{\gamma})^{\frac{1}{\gamma}-1}(1-\gamma+\gamma E^\gamma)}{(E^\gamma+(1-E^\gamma)^\frac{1}{\gamma})^2}
\end{pmatrix}
\end{equation}.


\section{Stabilit\'e du syst\`eme}

\subsection{Recherche des points d'\'equilibre}

Si $C = C_{max}$, on fige la valeur de C ainsi que tout le syst\`eme. On obtient donc un premier point d'\'equilibre.

Cherchons maintenant les points d'\'equilibres du syst\`eme lorsque $C<C_{max}$. On cherche donc $\textbf{x}$ et $\textbf{u}$ tels que :

\begin{equation}
 f(\textbf{x}(t), \textbf{u}, t)
=
\begin {pmatrix}
 0 & 0 & 0
\end {pmatrix}^T
\end{equation}

o\`u $f$ est d\'efinie par l'\'equation \ref{eq:f}.

On remarque que la condition $\frac{dC}{dt}=0$ suffit. $A_{max}$ \'etant non nul par hypot\`ese, on veut $E_{res}V = 0$. Or $V = 0$ implique que l'\'eleve n'accorde aucun int\'er\^et au cours, on a donc $C = C_{min} = 0$. Ce cas \'etant irr\'ealiste, nous ne le d\'etaillerons pas plus.

Ainsi
$E_{res} = 0
\Leftrightarrow E = 0
\Leftrightarrow N = 0$.
Or $N$ est positif et croissant en fonction de $C$. On a donc n\'ecessairement $C = C_{min}$.
 
Finalement, on observe un point d'\'equilibre pour 
$\textbf{x} = 
\begin {pmatrix}
C_{min} & 0 & 0
\end {pmatrix}^T
$.

\subsection{Stabilit\'e des points d'\'equilibre}

Pour les points d'\'equilibre o\`u $C<C_{max}$, on remarque que pour $C>C_{min}$, $N > 0$. Cela implique que $\frac{dC}{dt}>0$ et donc que $C$ est croissante. Ainsi, ces points d'\'equilibres sont instables.

Pour $C = C_{max}$, on rappelle que $C$, $E$ et $E_{res}$ sont croissants, major\'es et atteignent leurs bornes sup\'erieures respectives quand $C = C_{max}$. Ainsi, soit $\epsilon > 0$ et $t_{0}>0$, en possant $\delta = \epsilon$ on a :
$$||\bar{x} - x(t_{0})|| < \delta \Rightarrow ||\bar{x} - x(t)|| < \epsilon, \forall t>t_{0}$$
D'o\`u la stabilit\'e au sens de Lyapunov. On a de plus la stabilit\'e asymptotique car $\lim\limits_{t \rightarrow +\infty} || x(t) - \bar{x}|| = 0$.
