from math import log, exp
from matplotlib import pyplot as plt
from tkinter import *

def f(x,u,t):
    '''
    Calcul les dx/dt = f(x,u,t) selon la représentation d'état
    
    Parameters :
    x = (C,E,Eres) : (connaissance, probabilité de réussite, probabilité ressentie), variable d'état
    u = (Z, Cmax, Amax, Gamma, Nmin, Nmax, V, D, T) : variable d'entrée
    
    Return :
    (dC/dt, dE/dt, dEres/dt) : variation de chaque terme de x
    '''
    
    C, E, Eres = x
    Z, Cmax, Amax, Gamma, Nmin, Nmax, V, D, T = u
    #N = Niveau(Nmin, Nmax, N)
    
    Vres = V #pour l'instant

    dCdt = (Amax*Eres*Vres)/(Z + Gamma*(T-t))

    alpha = log(4)
    dEdt = (1-E)*dCdt*(alpha*(Nmax-Nmin))/(D*Cmax)

    gamma = 0.6
    dEresdt = (dEdt*(E**(gamma-1))*((1-E**gamma)**(1/gamma - 1))*(1+gamma-gamma*(E**gamma)))/((E**gamma + (1 - E**gamma)**(1/gamma))**2)

    return (dCdt, dEdt, dEresdt)


#Fonctions calculs

def Niveau(Nmin, Nmax, C, Cmax, N0 = 1):
    '''
    Calcul le niveau de l'élève en fonction de ses connaissances
    
    Parameters :
    Nmin : niveau minimal requis pour comprendre le cours
    Nmax : niveau maximal atteint une fois le cours appris
    C : niveau de connaissance du cours
    Cmax : niveau de connaissance paximal du cours
    N0 : niveau initial
    
    Return :
    N : niveau de l'élève
    '''
    return Nmin + C*(Nmax-Nmin)/Cmax + (N0 - Nmin) * (Cmax - C)/Cmax

def Expectancy (N,D):
    '''
    Calcul la probabilité E que l'élève a à réussir l'apprentissage selon la loi E = 1 - exp(-N/D)
    
    Parameters :
    N : Niveau de l'élève
    D : Difficulté du travail
    
    Return :
    E : probabilité de réussite
    '''
    return 1 - exp(-log(4) * N/D)

def Expectancy_res (E):
    '''
    Calcul la probabilité de réussite ressentie par l'élève avec la formule Eres = (E**g)/(E**g + (1-E**g)**(1/g))
    où g = 0.6
    
    Parameters :
    E : probabilité de réussite
    
    Return :
    Eres : probabilité de réussite perçue par l'élève
    '''
    return (E**0.6)/(E**0.6 + (1-E**0.6)**(1/0.6))

############### Tracé de la courbe ###############

def affiche_courbe():
    '''
    Trace la courbe en fonction de des paramètres donné par le simulateur
    
    Parameters :
    Pas d'entrée
    
    Return:
    Aucun
    '''
    #Initialisation de u (variables d'entrées)
    V = int(Value.var.get())
    D = int(Difficulty.var.get())
    T = int(Deadline.var.get())
    Gamma = int(Impulsivity.var.get())
    
    #Paramètres avancés
    Nmin = 1 #niveau minimal requis pour aborder le cours
    Nmax = 7 #niveau maximal atteint en connaissant le cours
    Cmax = 100 #niveau de connaissance maximal sur le cours (ici 100% du cours peut-être connu)
    Amax = 50 #niveau maximal d'appreentissage
    Z = 10 # =Vmax pour avoir 0 <= motivation <= 1
    
    uM = (Z, Cmax, Amax, Gamma, Nmin, Nmax, V, D, T) #Vecteur d'entré


    #Initialisation de x (vecteur d'état)
    C = 0 #On considère que l'élève avait une connaissance nulle du cours avant de le commencer
    N = Niveau(Nmin, Nmax, C, Cmax)
    E = Expectancy(N,D)
    E_res = Expectancy_res(E)
    x = (C, E, E_res)

    #Tracé de la courbe en utilisant la méthode d'Euler

    dt = 0.1 #On choisit ici un pas de temps de 0.1

    y = [C]
    t = [T]

    while t[-1]>0: #On arrête quand les mesures quand il ne reste plus de temps à l'élève pour apprendre son cours
        t.append(t[-1] - dt)
        dCdt, dEdt, dEresdt = f(x,uM,T-t[-1]) #Calcul de dx/dt
        C = C + dt * dCdt
        if C>Cmax:
            C = Cmax
        E = E + dt * dEdt
        E_res = E_res + dt * dEresdt
        x = (C,E,E_res)
        y.append(C)


    ######## Cours de dynamique
    #On applique la même méthode que pour le cours de modelisation

    T = int(Deadline_D.var.get())
    Nmin = Niveau(Nmin,Nmax,C,Cmax)
    Nmax = 15
    D2 =int(Difficulty_D.var.get())
    uD = (Z, Cmax, Amax, Gamma, Nmin, Nmax, V, D2, T)
    
    C_atteint = C #Sauvegarde le niveau de connaissance aquis en modélisation
    Cmaxi = Cmax
    Cmax = Cmax + Cmax - C_atteint
    C = 0 #On considère que l'élève avait une connaissance nulle du cours avant de le commencer
    N = Niveau(Nmin, Nmax, C, Cmax)
    E = Expectancy(N,D2)
    E_res = Expectancy_res(E)
    x = (C, E, E_res)
        
    y2 = [C]
    t2 = [T]

    while t2[-1]>0: #On arrête quand les mesures quand il ne reste plus de temps à l'élève pour apprendre son cours
        t2.append(t2[-1] - dt)
        N = Niveau(Nmin, Nmax, C, Cmax)
        if C < Cmaxi - C_atteint :
            u = uM
        else :
            u = uD
        dCdt, dEdt, dEresdt = f(x,u,T-t2[-1]) #Calcul de dx/dt
        C = C + dt * dCdt
        if C>Cmax:
            C = Cmax
        E = E + dt * dEdt
        E_res = E_res + dt * dEresdt
        x = (C,E,E_res)
        y2.append(C)

    for i in range(len(y2)):
        y2[i] -= Cmaxi - C_atteint
    #Tracé des courbes
    
    fig, (cours_mode, cours_dyn) = plt.subplots(1,2)
    
    cours_mode.plot(t,y)
    cours_mode.set(xlabel = 'Durée restante', ylabel = 'Pourcentage de connaissance du cours')
    cours_mode.invert_xaxis()
    cours_mode.set_title('Modélisation')
    
    cours_dyn.plot(t2,y2, 'tab:green')
    cours_dyn.set(xlabel = 'Durée restante')
    cours_dyn.invert_xaxis()
    cours_dyn.set_title('Commande des Systèmes Dynamiques')
    
    plt.show()


############### Interface Graphique ###############


# Creation d'une nouvelle classe pour ne pas provoquer une erreure en cas de mauvaise utilisation du simulateur

class Int_entry(Entry): #Une sous-classe de Entry qui ne peut prendre que des entiers
    
    def __init__(self, master=None, first_number = 1, min = 0,max = 10, **kwargs):
        self.var = StringVar()
        Entry.__init__(self, master, textvariable=self.var, **kwargs)
        self.var.set(str(first_number))
        self.var.trace('w', self.check)
        self.min = min
        self.max = max
        
    def check(self, *args):
        if self.var.get().isdigit() or self.var.get() == '': 
            self.old_value = self.get()
        else:
            self.var.set(self.old_value)
        if self.var.get() != '':
            if int(self.var.get()) < self.min:
                self.var.set(self.min)
                self.old_value = self.get()
            elif int(self.var.get()) > self.max:
                self.var.set(self.max)
                self.old_value = self.get()


# Début de l'interface

root = Tk()
root.title("Simuler la motivation d'apprendre")
root.focus()

label_title = Label(root, text = 'Simulateur', font=20)
label_title.pack()

title_canvas = Canvas(root,width=200, height=2, bg = "#111111")
title_canvas.pack()


frm = Frame(root)

#Variables d'entrées:

Label(frm, text="Valeur apportée au cours").grid(row=0, column = 1)
Value = Int_entry(frm,first_number=5)
Value.grid(row = 0, column=2)

Label(frm, text="Impulsivité").grid(row=1, column=1)
Impulsivity = Int_entry(frm,first_number=3)
Impulsivity.grid(row = 1, column=2)


Label(frm, text = '''Deadline du cours de
      Modélisation''').grid(row=2)
Deadline = Int_entry(frm,first_number=60, max = float('inf'))
Deadline.grid(row = 2, column=1)

Label(frm, text='''Difficulté du cours de
      Modélisation''').grid(row=3)
Difficulty = Int_entry(frm,first_number=25, max = 100, min = 1)
Difficulty.grid(row =3, column=1)

Label(frm, text = '''Deadline du cours de
      Commandes des sytèmes dynamiques''').grid(row=2, column=2)
Deadline_D = Int_entry(frm,first_number=60, max = float('inf'))
Deadline_D.grid(row = 2, column=3)

Label(frm, text='''Difficulté du cours de
      Commandes des systèmes dynamiques''').grid(row=3, column=2)
Difficulty_D = Int_entry(frm,first_number=37, max = 100, min = 1)
Difficulty_D.grid(row = 3, column=3)


frm.pack()

#Expluication

def show_explanations():
    explanation = Toplevel()
    explanation.title("Explications")
    explanation.focus()
    
    label_intro = Label(explanation, text = "Nous chechons ici à simuler la motivation d'apprendre un cours")
    label_intro.grid(row = 0, column=0, columnspan=2)
    
    Label(explanation, text="Deadline : ").grid(row = 1, column=0)
    label = Label(explanation,
                        text = '''
                        représente, en jour, le temps dont l'élève dispose
                        pour apprendre son cour
                        ''')
    label.grid(row = 1, column=1)
    
    Label(explanation, text="Valeur apportée au cours : ").grid(row = 2, column=0)
    label = Label(explanation,
                        text = '''
                        représente la valeur que l'élève donne au cours (0
                        l'éleve n'accordera aucun intérêt et ne travaillera
                        pas, 10 l'élève accordera énormément d'intérêt au 
                        travail)
                        ''')
    label.grid(row = 2, column=1)
    
    Label(explanation, text="Difficulté : ").grid(row = 3, column=0)
    label = Label(explanation,
                        text = '''
                        représente la difficulté du cours (1 le cours est 
                        extrêment simple, 100 le cours est presque imposssible
                        à comprendre)
                        ''')
    label.grid(row = 3, column=1)
    
    Label(explanation, text="Impulsivité : ").grid(row = 4, column=0)
    label = Label(explanation,
                        text = '''
                        représente le côté impulsif de l'élève, c'est-à-dire,
                        la facilité qu'il a à se déconcentrer et faire autre
                        chose (0 l'élève ne se laissera jamais distraire, 10
                        il aura énormément de mal à travailler)
                        ''')
    label.grid(row = 4, column=1)
    

explanation_button = Button(frm, text="Aide", command=show_explanations, width = 20)
explanation_button.grid(row = 4,column = 1)

#Lancement

bouton_lancement = Button(frm, text='Lancer simulation', command=affiche_courbe, width=20)
bouton_lancement.grid(row = 4, column=2)




root.mainloop()